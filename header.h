#ifndef QUADRATI_MAGICI_HEADER_H
#define QUADRATI_MAGICI_HEADER_H

#define MAX_ROW     100
#define MAX_COLUMN  100


#include <stdio.h>
#include <time.h>  /* for time() */
#include <stdlib.h>  /* for rand() and srand() */

int check_square(int square[][MAX_COLUMN], int );
void generate(int matrix[][MAX_COLUMN], int );

#endif //QUADRATI_MAGICI_HEADER_H
