# Quadrati Magici

Si vuole implementare un programma per la simulazione del gioco del quadrato magico. Un quadrato viene definito magico se la somma di ogni riga, ogni colonna e ogni diagonale è sempre lo stesso numero (costante magica). Ad esempio usando gli interi 1,2, ..., 9 il seguente è un quadrato magico con somma uguale a 15:

<p align="center">
    <img width="280" src="https://user-images.githubusercontent.com/26567236/40629031-ed8caa70-62c8-11e8-95a9-1d8f2005258f.png" alt="logo">
</p>

Sviluppare un programma che date le dimensioni del quadrato n x n e numeri interi da 1 a n2 verifichi se la matrice è un quadrato magico. Le matrici vengono generate con numeri casuali. Effettuare ogni test per 1000 volte visualizzando la percentuale di successi.
Effettuare almeno 3 simulazioni variando le dimensioni del quadrato.