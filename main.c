#include "header.h"

int main() {

    int size_matrix = 0, magic_square = 0, successi = 0;
    int matrix[MAX_ROW][MAX_COLUMN];

    do {

        printf("Inserire la dimensione del quadrato: ");
        scanf("%d", &size_matrix);

        if (size_matrix <= 1 ) {

            printf("Attenzione, scelta non valida la grandezza della matrice deve essere > 1\n");

        }

    } while (size_matrix <= 1);


    srand((unsigned int) time(0)); // genereting different sequence of number for each execution


    for (int i = 0; i < 1000; ++i) {

        generate(matrix, size_matrix); // creating random matrix

        if (check_square(matrix, size_matrix)) { //Check if the given matrix is a magic square

            magic_square++;
        };

    }

    //calculate percentage of success
    successi = magic_square / 1000 * 100;

    printf("La percentuale di successi è: %d %%", successi);

}
