#include "header.h"

/*
 *  Function: generate
 *  ----------------------------
 *  Returns (void):
 *
 *  input: @array2D  matrix [], int size (matrix)
 *
 *  Function per generare una matrice con numeri da 1-10 pseudocasuali
 *
 */

void generate (int matrix[][MAX_COLUMN], int size_matrix) {

    for ( int i = 0; i < size_matrix; i++) {


        for ( int j = 0; j < size_matrix; j++) {

            matrix[i][j] = (rand() % 10) + 1;

        }
    }

}


/*
 *  Function: check square
 *  ----------------------------
 *  Returns (int): return int 0 se la matrice in input non è un quadrato magico altrimenti return 1.
 *
 *  input: @array2D  matrix [], int size (matrix)
 *
 *  Function per controllare se la matrice in input è un quadrato magico.
 *
 */


int check_square(int square[][MAX_COLUMN], int size ){

    int rowsum, columnsum, diagonalsum = 0, diagonalminsum = 0, compare = 0;

    for(int i = 0; i < size; i++)
    {

        //calculate diagonal min and diagonal max
        diagonalsum += square[i][i];
        diagonalminsum += square[i][size-1-i];

        rowsum = 0;
        columnsum = 0;

        for(int j = 0; j < size; j++)
        {
            rowsum += square[i][j];
            columnsum += square[j][i];
        }

        //Get a copy of the first row
        if (i == 0) {

            compare = rowsum;

        }

        //Check if the rows and columns are all equal
        if (compare != rowsum || compare != columnsum) {

            return 0;

        }

    }

    if (diagonalsum != compare || diagonalminsum != compare) {

        return 0;

    }


    return 1;


};



